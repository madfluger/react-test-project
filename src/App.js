import React, { Component } from "react";
import { Route, Switch } from "react-router";

import StartPage from "./pages/StartPage";

class App extends Component {
  render() {
    return (
      <Switch>
        <Route path="/" component={StartPage} />
      </Switch>
    );
  }
}

export default App;
